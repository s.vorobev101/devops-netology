# Домашнее задание к занятию "5.1. Основы виртуализации"

## Задача 1

``` 
Вкратце опишите, как вы поняли - в чем основное отличие паравиртуализации и виртуализации на основе ОС.
```

Основное отличие - отсутствие прослойки гипервизора для виртуализации на основе ОС. 
По сути, виртуалка позволяет эмулировать железо в то время как контейнер эмулирует ОС. 
Отсюда вытекает ряд особенностей:
- гипервизор позволяет запускать виртуалку с ОС, отличной, от основной, на которой крутится сам гипервизор. Потому с запуском контейнеров на с отличающимися ОС возникают свои особенности
- Отсутствие прослойки оптимизирует обращение к основной ОС и, как следствие, и ресурсам железа
- Все виртуалки так или иначе обращаются к одному и тому же гипервизору, в то время как контейнеры полностью изолированы друг от друга
- Виртуализация на основе ОС позволяет использовать один экземпляр некой библиотеки для всех экземпляров контейнера в рамках конкретного слоя, снижая расход памяти

Также различна цель технологии: контейнеры решают более точечные конкретные запросы и представляют собой минимально необходимый набор инфраструктуры для работы нужного сервиса, в то время как виртуалка намного ближе к обычной рабочей станции с кучей дополнительных вспомогательных и не всегда используемых приложений и библиотек.

При этом, если полная виртуализация представляет собой полноценную машину, где вообще все вызовы идут через гипервизор, установлена ОС, полностью аналогичная ОС на железном сервере, то паравиртуализация допускает прямой доступ к некоторым ресурсам хоста. Как следствие, для паравиртуализации используются несколько модифицированные версии ОС,
что ощутимо снижает список доступных ОС для разворачивания на ВМ. 
 
## Задача 2
```
Выберите тип один из вариантов использования организации физических серверов, 
в зависимости от условий использования.

Организация серверов:
- физические сервера
- паравиртуализация
- виртуализация уровня ОС

Условия использования:

- Высоконагруженная база данных, чувствительная к отказу
- Различные Java-приложения
- Windows системы для использования Бухгалтерским отделом 
- Системы, выполняющие высокопроизводительные расчеты на GPU

Опишите, почему вы выбрали к каждому целевому использованию такую организацию.
```
> - Высоконагруженная база данных, чувствительная к отказу

физические сервера, либо паравиртуализация. Физические даже предпочтительнее, потому что
- Нужно свести к минимуму все накладные расходы на работу БД (гипервизор)
- Невысокая необходимость к клонированию сервера. Резервирование же можно обеспечить и без виртуализации. 

С другой стороны, виртуализация позволит лучше контролировать расход системных ресурсов и повысит безопасность

> - Различные Java-приложения

Звучит весьма обще, но, пожалуй, тут, в первую очередь, просятся контейнеры: общие библиотеки, легкость администрирования и масштабирования. 
Паравиртуализация тоже подойдёт, но понадобится чуть больше ресурсов.

> - Windows системы для использования Бухгалтерским отделом 

Паравиртуализация: высокой нагрузки не ожидается, дополнительная безопасность, возможность быстро добавлять и удалять новые сервера. 
К тому же, тут явно не предполагается использование максимально усечённой версии ОС с минимумом библиотек. 
Ну и смешение ОС говорит против выбора контейнеров. 
Физические сервера тут не нужны вовсе.

> - Системы, выполняющие высокопроизводительные расчеты на GPU

Вот тут, пожалуй, все 3 варианта могут подойти: физические сервера обеспечат минимум накладных расходов, а раз мы будем максимально грузить GPU, то масштабирование тут особо не предполагается (в любом случае придётся физически добавлять мощностей GPU).
Паравиртуализация и контейнеризация позволят проще масштабировать количество систем и регулировать распределение доступных ресурсов, если такая необходимость возникнет.

## Задача 3

```
Как вы думаете, возможно ли совмещать несколько типов виртуализации на одном сервере?
Приведите пример такого совмещения.
```

Можно. С примерами сложнее, первое, что приходит на ум: поднятие контейнеров с ОС, отличной от ОС сервера, для чего может понадобится прослойка виртуальной машины, чтобы эмулировать ресурсы железа. 
Возможно, в каких-то случаях, нужно выполнять масштабирование контейнеров за счёт клонирования виртуалок.

## 1
> Мы выгрузили JSON, который получили через API запрос к нашему сервису:

```json
{ "info" : "Sample JSON output from our service\t",
    "elements" :[
        { "name" : "first",
        "type" : "server",
        "ip" : 7175 
        },
        { "name" : "second",
        "type" : "proxy",
        "ip : 71.78.22.43
        }
    ]
}
```
> Нужно найти и исправить все ошибки, которые допускает наш сервис

IP для first, конечно, кривой, но у нас нет ни корректных данных, ни правил валидации, потому оставляем as is с переводом в строку.
```json
{
  "info": "Sample JSON output from our service ",
  "elements": [
    {
      "name": "first",
      "type": "server",
      "ip": "7175"
    },
    {
      "name": "second",
      "type": "proxy",
      "ip": "71.78.22.43"
    }
  ]
}
```

## 2 
> В прошлый рабочий день мы создавали скрипт, позволяющий опрашивать веб-сервисы и получать их IP. К уже реализованному функционалу нам нужно добавить возможность записи JSON и YAML файлов, описывающих наши сервисы. Формат записи JSON по одному сервису: { "имя сервиса" : "его IP"}. Формат записи YAML по одному сервису: - имя сервиса: его IP. Если в момент исполнения скрипта меняется IP у сервиса - он должен так же поменяться в yml и json файле.

```python
import json
import yaml
import sys
from socket import gethostbyname

try:
    mode = sys.argv[1]
except IndexError:  # default mode just prints out the DNS table
    print("You could use additional argument: '1' - check IP mismatches\n"
          "'2' - check IP mismatches and print the DNS table")
    mode = "2"

hosts_json = "hosts.json"
hosts_yml = "hosts.yml"

# read current configuration. Just one of two (JSON/YAML) is enough since they're synchronized
try:
    with open(hosts_json, "r") as of:
        hosts = json.loads(of.read())
except FileNotFoundError:
    hosts = {"drive.google.com": "", "mail.google.com": "", "google.com": ""}


def get_ip_by_url(url):
    return gethostbyname(url)


def update_ip(update_file=True, print_addresses=False, check_prev_entry=False):
    """process current IP addresses for stored URLs"""
    new_updates = False
    for k, v in hosts.items():  # check each server for new IP addresses
        ip = get_ip_by_url(k)
        if ip != v:
            new_updates = True
            if check_prev_entry:
                print(f"[ERROR] {k} IP mismatch: {v}, {ip}")
        hosts[k] = ip
    if check_prev_entry and not new_updates:
        print(f"IP check finished: no mismatches")
    if update_file and new_updates:  # updated local json and yaml files, storing DNS table
        with open(hosts_json, "w") as wf:
            json.dump(hosts, wf)
        with open(hosts_yml, "w") as wf:
            yaml.dump(hosts, wf)
    if print_addresses:
        for k, v in hosts.items():
            print(f"{k} - {v}")


if __name__ == "__main__":
    if mode == "0":
        update_ip(print_addresses=True)
    if mode == "1":
        update_ip(check_prev_entry=True)
    if mode == "2":
        update_ip(print_addresses=True, check_prev_entry=True)

```

## 3
> Так как команды в нашей компании никак не могут прийти к единому мнению о том, какой формат разметки данных использовать: JSON или YAML, нам нужно реализовать парсер из одного формата в другой. Он должен уметь:
> - Принимать на вход имя файла
> - Проверять формат исходного файла. Если файл не json или yml - скрипт должен остановить свою работу
> - Распознавать какой формат данных в файле. Считается, что файлы *.json и *.yml могут быть перепутаны
> - Перекодировать данные из исходного формата во второй доступный (из JSON в YAML, из YAML в JSON)
> - При обнаружении ошибки в исходном файле - указать в стандартном выводе строку с ошибкой синтаксиса и её номер
> - Полученный файл должен иметь имя исходного файла, разница в наименовании обеспечивается разницей расширения файлов

```python
import sys
import json
import yaml
import yaml.parser


class ValidationError(Exception):
    pass


# get file name
try:
    incoming_file = sys.argv[1]
except IndexError:
    raise ValidationError("Please provide .json or .yml file")

with open(incoming_file, "r") as of:  # raise default FileNotFoundError if no file found
    read_file = of.read()


clean_file_name = incoming_file.split(".")[:-1]  
clean_file_name = ".".join(clean_file_name)  # filename without extension


def convert_file():
    """detect json/yaml file type and convert to the other one """
    try:
        data = json.loads(read_file)  # convert to yaml if json
        with open(f"{clean_file_name}.yml", "w") as wf:
            yaml.dump(data, wf)
        print(f"Successfully converted {incoming_file} to {clean_file_name}.yml")
        return
    except json.JSONDecodeError as j_exc:
        json_exc = j_exc
    try:
        data = yaml.safe_load(read_file)  # convert to json if yaml
        with open(f"{clean_file_name}.json", "w") as wf:
            json.dump(data, wf)
        print(f"Successfully converted {incoming_file} to {clean_file_name}.json")
        return
    except yaml.parser.ParserError as y_exc:
        yaml_exc = y_exc
    # if both conversions failed, output exceptions
    print(f"unable to determine {incoming_file} file format:\nnot a proper JSON - {json_exc}\nnot a proper YAML - {yaml_exc}")


convert_file()

```
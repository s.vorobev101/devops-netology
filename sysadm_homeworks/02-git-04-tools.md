1. Найдите полный хеш и комментарий коммита, хеш которого начинается на aefea.
   ```
   S C:\Users\username\PycharmProjects\terraform> git show aefea --format="%H %s"  -s
   aefead2207ef7e2aa5dc81a34aedf0cad4c32545 Update CHANGELOG.md
   ```

1. Какому тегу соответствует коммит 85024d3?
   ```
   PS C:\Users\username\PycharmProjects\terraform> git show 85024d3 --format="%D" -s
   tag: v0.12.23
   ```
   
1. Сколько родителей у коммита b8d720? Напишите их хеши.
   ```
   PS C:\Users\username\PycharmProjects\terraform> git show b8d720^ --format="%H"
   56cd7859e05c36c06b56d013b55a252d0bb7e158
   ```

1. Перечислите хеши и комментарии всех коммитов которые были сделаны между тегами v0.12.23 и v0.12.24.
   ```
   PS C:\Users\username\PycharmProjects\terraform> git log --pretty=oneline v0.12.23...v0.12.24
   33ff1c03bb960b332be3af2e333462dde88b279e (tag: v0.12.24) v0.12.24
   b14b74c4939dcab573326f4e3ee2a62e23e12f89 [Website] vmc provider links
   3f235065b9347a758efadc92295b540ee0a5e26e Update CHANGELOG.md
   6ae64e247b332925b872447e9ce869657281c2bf registry: Fix panic when server is unreachable
   5c619ca1baf2e21a155fcdb4c264cc9e24a2a353 website: Remove links to the getting started guide's old location
   06275647e2b53d97d4f0a19a0fec11f6d69820b5 Update CHANGELOG.md
   d5f9411f5108260320064349b757f55c09bc4b80 command: Fix bug when using terraform login on Windows
   4b6d06cc5dcb78af637bbb19c198faff37a066ed Update CHANGELOG.md
   dd01a35078f040ca984cdd349f18d0b67e486c35 Update CHANGELOG.md
   225466bc3e5f35baa5d07197bbc079345b77525e Cleanup after v0.12.23 release
   ```

1. Найдите коммит в котором была создана функция func providerSource, ее определение в коде выглядит так func providerSource(...) (вместо троеточего перечислены аргументы).
   ```
   PS C:\Users\username\PycharmProjects\terraform> git log -S "func providerSource" --oneline
   5af1e6234 main: Honor explicit provider_installation CLI config when present
   8c928e835 main: Consult local directories as potential mirrors of providers
   ```
   логично предположить, что функция была добавленна в самом раннем коммите 8c928e835, но вседа можно проверить с помощью git show 8c928e835
 
1. Найдите все коммиты в которых была изменена функция globalPluginDirs.
   ```
   # сначала найти файл, где определена функция
   
   PS C:\Users\username\PycharmProjects\terraform> git grep -p globalPluginDirs
   commands.go=func initCommands(
   commands.go:            GlobalPluginDirs: globalPluginDirs(),
   commands.go=func credentialsSource(config *cliconfig.Config) (auth.CredentialsSource, error) {
   commands.go:    helperPlugins := pluginDiscovery.FindPlugins("credentials", globalPluginDirs())
   internal/command/cliconfig/config_unix.go=func homeDir() (string, error) {
   internal/command/cliconfig/config_unix.go:              // FIXME: homeDir gets called from globalPluginDirs during init, before
   plugins.go=import (
   plugins.go:// globalPluginDirs returns directories that should be searched for
   plugins.go:func globalPluginDirs() []string {
   
   # и запустутить поиск изменений 
   PS C:\Users\username\PycharmProjects\terraform> git log -s -L :globalPluginDirs:plugins.go --format=oneline
   78b12205587fe839f10d946ea3fdc06719decb05 Remove config.go and update things using its aliases
   52dbf94834cb970b510f2fba853a5b49ad9b1a46 keep .terraform.d/plugins for discovery
   41ab0aef7a0fe030e84018973a64135b11abcd70 Add missing OS_ARCH dir to global plugin paths
   66ebff90cdfaa6938f26f908c7ebad8d547fea17 move some more plugin search path logic to command
   8364383c359a6b738a436d1b7745ccdce178df47 Push plugin discovery down into command package
   ```

1. Кто автор функции synchronizedWriters?

   Не удалось найти функцию в репозитории:
   ```
   PS C:\Users\username\PycharmProjects\terraform> git grep -p synchronizedWriters
   # вообще нет вхождений этой строки
   C:\Users\username\PycharmProjects\terraform> git grep synchronizedWriters
   # в сообщениях коммитов тоже пустота
   C:\Users\username\PycharmProjects\terraform> git log --all --grep="synchronizedWriters"
   ```
 
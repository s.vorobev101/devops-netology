# Домашнее задание к занятию "4.1. Командная оболочка Bash: Практические навыки"

## Обязательные задания

1. Есть скрипт:
	```bash
	a=1
	b=2
	c=a+b
	d=$a+$b
	e=$(($a+$b))
	```
	* Какие значения переменным c,d,e будут присвоены?
	* Почему?

    ```shell script
    echo $c
    a+b
    # интерпретируем не как переменные, а как строковые значения
    ```
    ```shell script
    echo $d
    1+2
    # интепретируем как строковые переменные
    ```
    ```shell script
    echo $e
    3
    # интепретируем как переменные и присваиваем e значение всего выражения
    ```

1. На нашем локальном сервере упал сервис и мы написали скрипт, который постоянно проверяет его доступность, записывая дату проверок до тех пор, пока сервис не станет доступным. В скрипте допущена ошибка, из-за которой выполнение не может завершиться, при этом место на Жёстком Диске постоянно уменьшается. Что необходимо сделать, чтобы его исправить:
	```bash
	while ((1==1)
	do
	curl https://localhost:4757
	if (($? != 0))
	then
	date >> curl.log
	fi
	done
	```

    - добавим условие выхода при условии успешного вызова
    - паузу в секунду при опросе
    - заглушим вывод curl, раз уж мы проверяем только код результата
    - ну и shebang не повредит
    ещё, по-хорошему, можно прикрутить ротацию файла лога, потому сервис может никогда не подняться
    ```shell script
    #!/usr/bin/env bash
    mon_pause=1  # p
    
    while ((1==1))
    do
      curl curl --silent --output /dev/null https://localhost:4757
      if (($? != 0))
      then
        date >> curl.log
        elif (($? == 0))
        then
          exit
        fi
        sleep $mon_pause  # probably no need to ping the service as fast as possible
    done
    ```
 
1. Необходимо написать скрипт, который проверяет доступность трёх IP: 192.168.0.1, 173.194.222.113, 87.250.250.242 по 80 порту и записывает результат в файл log. Проверять доступность необходимо пять раз для каждого узла.
    ```shell script
    #!/usr/bin/env bash
    
    s_list=("192.168.0.1" "173.194.222.113" "87.250.250.242")
    ping_times=5  # total curl attempts per host
    port=80
    timeout=5  #  each curl attempt timeout in seconds
    
    do
      for host in ${s_list[@]}
      do
        echo "testing $host:$port" >> curl.log
        i=1
        while (($i <= $ping_times))
        do
          curl -m $timeout --silent --output /dev/null http://$host:$port
          echo "attempt $i result $?" >> curl.log
          let "i += 1"
        done
    done
    
    ```

1. Необходимо дописать скрипт из предыдущего задания так, чтобы он выполнялся до тех пор, пока один из узлов не окажется недоступным. Если любой из узлов недоступен - IP этого узла пишется в файл error, скрипт прерывается

    - можно также добавить паузы между опросами серверов
    ```shell script
    #!/usr/bin/env bash
    
    s_list=("173.194.222.113" "87.250.250.242" "192.168.0.1")
    ping_times=5
    port=80
    timeout=5
    
    while ((1==1))
    do
        for host in ${s_list[@]}
        do
          echo "testing $host:$port" >> curl.log
          i=1
          while (($i <= $ping_times))
          do
            curl -m $timeout --silent --output /dev/null http://$host:$port
            res=$?
            echo "attempt $i result $res" >> curl.log
            if (($res != 0))
            then
              echo "$(date) $host" >> error.log
              exit
            fi
            let "i += 1"
          done
      done
    done
    ```
## 1
Есть скрипт:

```
#!/usr/bin/env python3
a = 1
b = '2'
c = a + b
```

> Какое значение будет присвоено переменной c?

получим TypeError

> Как получить для переменной c значение 12?

сконвертировать a в строку:
`c = str(a) + b`
если нужен именно int, то сконвертировать результат конкатенации `int(c)`

> Как получить для переменной c значение 3?

сконвертировать строку в int: `c = a + int(b)`

## 2
> Мы устроились на работу в компанию, где раньше уже был DevOps Engineer. Он написал скрипт, позволяющий узнать, какие файлы модифицированы в репозитории, относительно локальных изменений. Этим скриптом недовольно начальство, потому что в его выводе есть не все изменённые файлы, а также непонятен полный путь к директории, где они находятся. Как можно доработать скрипт ниже, чтобы он исполнял требования вашего руководителя?
```python
#!/usr/bin/env python3

import os

bash_command = ["cd ~/netology/sysadm-homeworks", "git status"]
result_os = os.popen(' && '.join(bash_command)).read()
is_change = False
for result in result_os.split('\n'):
    if result.find('modified') != -1:
        prepare_result = result.replace('\tmodified:   ', '')
        print(prepare_result)
        break
```

у нас есть путь репозитория относительно директории, из которой запускается скрипт. 
Так что просто добавим getcwd(). Вопросы проверки существования директории и того, что директория является git-репозиторием отложим на потом.
Ну и break в конце цикла не нужен совсем
```python
import os

rep_path = os.getcwd() + "/netology/sysadm-homeworks"
bash_command = ["cd ~/netology/sysadm-homeworks", "git status"]
result_os = os.popen(' && '.join(bash_command)).read()
is_change = False
print(f"repository full path: {rep_path}")
for result in result_os.split('\n'):
    if result.find('modified') != -1:
        prepare_result = result.replace('\tmodified:   ', '')
        print(prepare_result)
```

## 3
> Доработать скрипт выше так, чтобы он мог проверять не только локальный репозиторий в текущей директории, а также умел воспринимать путь к репозиторию, который мы передаём как входной параметр. Мы точно знаем, что начальство коварное и будет проверять работу этого скрипта в директориях, которые не являются локальными репозиториями.

Добавляем чтение первого аргумента и хэндлер, на случай, если он не был передан. os заменили на subprocess, чтобы лучше обрабатывать возможные ошибки в stderr (директория не существует, не является git-репозиторием, ОС не знает команды git и т.д.)
Ну и автодект кодовой страницы для большей кросс-системности. 
```python
import sys
import subprocess
from chardet import detect


class ValidationError(Exception):
    pass


# read incoming parameters
try:
    rep_path = sys.argv[1]
except IndexError:
    raise ValidationError("please provide absolute repository path as the 1st argument")


def show_modified_files(rep_path):
    """prints out local modified files against provided repository"""
    bash_command = [f"cd {rep_path}", "git status"]
    ret = subprocess.Popen(' && '.join(bash_command), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = ret.communicate()
    if err:
        enc = get_encoding(err)
        print(f"unable to run command \n {bash_command}\nError: {err.decode(enc)}")
        return
    enc = get_encoding(out)
    result_os = out.decode(enc)
    print(f"repository full path: {rep_path}")
    for result in result_os.split('\n'):
        if result.find('modified') != -1:
            prepare_result = result.replace('\tmodified:   ', '')
            print(prepare_result)


def get_encoding(raw_data):
    """returns auto-detected codepage name from byte data"""
    enc = detect(raw_data)
    return enc["encoding"]


if __name__ == "__main__":
    show_modified_files(rep_path=rep_path)
```

## 4
> Наша команда разрабатывает несколько веб-сервисов, доступных по http. Мы точно знаем, что на их стенде нет никакой балансировки, кластеризации, за DNS прячется конкретный IP сервера, где установлен сервис. Проблема в том, что отдел, занимающийся нашей инфраструктурой очень часто меняет нам сервера, поэтому IP меняются примерно раз в неделю, при этом сервисы сохраняют за собой DNS имена. Это бы совсем никого не беспокоило, если бы несколько раз сервера не уезжали в такой сегмент сети нашей компании, который недоступен для разработчиков. Мы хотим написать скрипт, который опрашивает веб-сервисы, получает их IP, выводит информацию в стандартный вывод в виде: <URL сервиса> - <его IP>. Также, должна быть реализована возможность проверки текущего IP сервиса c его IP из предыдущей проверки. Если проверка будет провалена - оповестить об этом в стандартный вывод сообщением: [ERROR] <URL сервиса> IP mismatch: <старый IP> <Новый IP>. Будем считать, что наша разработка реализовала сервисы: drive.google.com, mail.google.com, google.com.

наверняка можно проще:)
Дефолтный режим работы скрипта - вывод DNS-таблицы. Можно передать дополнительный аргумент для вывода mismatched записей. Сравнение с "предыдущим" IP-адресом реализовано через хранимые в отдельном JSON данные. Можно ещё добавить туда таймстемп, чтобы в случае мисматча выводить ещё и время последнего обновления DNS таблицы, чтобы понимать за какой период сменился IP, но покамест это кажется несколько излишним.
```python
import json
import sys
from socket import gethostbyname

try:
    mode = sys.argv[1]
except IndexError:  # default mode just prints out the DNS table
    print("You could use additional argument: '1' - check IP mismatches\n"
          "'2' - check IP mismatches and print the DNS table")
    mode = "0"

hosts_file = "hosts.json"

# read current configuration
try:
    with open(hosts_file, "r") as of:
        hosts = json.loads(of.read())
except FileNotFoundError:
    hosts = {"drive.google.com": "", "mail.google.com": "", "google.com": ""}


def get_ip_by_url(url):
    return gethostbyname(url)


def update_ip(update_file=True, print_addresses=False, check_prev_entry=False):
    """process current IP addresses for stored URLs"""
    new_updates = False
    for k, v in hosts.items():  # check each server for new IP addresses
        ip = get_ip_by_url(k)
        if ip != v:
            new_updates = True
            if check_prev_entry:
                print(f"[ERROR] {k} IP mismatch: {v}, {ip}")
        hosts[k] = ip
    if check_prev_entry and not new_updates:
        print(f"IP check finished: no mismatches")
    if update_file and new_updates:  # updated local json file, storing DNS table
        with open(hosts_file, 'w') as wf:
            json.dump(hosts, wf)
    if print_addresses:
        for k, v in hosts.items():
            print(f"{k} - {v}")


if __name__ == "__main__":
    if mode == "0":
        update_ip(print_addresses=True)
    if mode == "1":
        update_ip(check_prev_entry=True)
    if mode == "2":
        update_ip(print_addresses=True, check_prev_entry=True)
```